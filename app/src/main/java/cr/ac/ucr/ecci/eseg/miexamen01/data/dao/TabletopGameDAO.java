package cr.ac.ucr.ecci.eseg.miexamen01.data.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import cr.ac.ucr.ecci.eseg.miexamen01.data.database.DataBaseContract.TabletopGameEntry;
import cr.ac.ucr.ecci.eseg.miexamen01.data.database.DataBaseHelper;
import cr.ac.ucr.ecci.eseg.miexamen01.data.model.Publisher;
import cr.ac.ucr.ecci.eseg.miexamen01.data.model.TabletopGame;

/**
 * @author Daniel Marín M
 */
public class TabletopGameDAO implements DAO<TabletopGame> {
    private static final String TAG = "TableTopGameDAO";

    private DataBaseHelper dataBaseHelper;

    public TabletopGameDAO(Context context) {
        this.dataBaseHelper = new DataBaseHelper(context);
    }

    @Override
    public List<TabletopGame> getAll() {
        PublisherDAO publisherDAO = new PublisherDAO(dataBaseHelper);
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();
        List<TabletopGame> tabletopGames = new ArrayList<>();
        Cursor cursor = db.query(
                TabletopGameEntry.TABLE_NAME,
                TabletopGame.getProjection(),
                null,
                null,
                null,
                null,
                null
        );

        if (cursor.moveToFirst() && cursor.getCount() > 0) {
            do {
                String id = (cursor.getString(cursor.getColumnIndexOrThrow(TabletopGameEntry.COLUMN_ID)));
                String name = (cursor.getString(cursor.getColumnIndexOrThrow(TabletopGameEntry.COLUMN_NAME)));
                String year = (cursor.getString(cursor.getColumnIndexOrThrow(TabletopGameEntry.COLUMN_YEAR)));
                String publisher_name = (cursor.getString(cursor.getColumnIndexOrThrow(TabletopGameEntry.COLUMN_PUBLISHER)));
                String description = (cursor.getString(cursor.getColumnIndexOrThrow(TabletopGameEntry.COLUMN_DESCRIPTION)));
                String players = (cursor.getString(cursor.getColumnIndexOrThrow(TabletopGameEntry.COLUMN_PLAYERS)));
                Integer ages = (cursor.getInt(cursor.getColumnIndexOrThrow(TabletopGameEntry.COLUMN_AGES)));
                String playingTime = (cursor.getString(cursor.getColumnIndexOrThrow(TabletopGameEntry.COLUMN_PLAYINGTIME)));

                Publisher publisher = publisherDAO.get(publisher_name);
                tabletopGames.add(new TabletopGame(id, name, year, publisher, description, players, ages, playingTime));
            } while (cursor.moveToNext());
            Log.i(TAG, cursor.getCount() + (cursor.getCount() == 1 ? " row" : " rows") + " retrieved from query");
        } else {
            Log.i(TAG, "No Tabletop game was retrieved from the query");
        }
        cursor.close();
        db.close();
        return tabletopGames;
    }

    @Override
    public TabletopGame get(String id) {
        PublisherDAO publisherDAO = new PublisherDAO(dataBaseHelper);
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();
        TabletopGame tabletopGame = null;
        String selection = TabletopGameEntry.COLUMN_ID + " = ?";
        String[] selectionArgs = {id};
        Cursor cursor = db.query(
                TabletopGameEntry.TABLE_NAME,
                TabletopGame.getProjection(),
                selection,
                selectionArgs,
                null,
                null,
                null
        );

        if (cursor.moveToFirst() && cursor.getCount() > 0) {
            String name = (cursor.getString(cursor.getColumnIndexOrThrow(TabletopGameEntry.COLUMN_NAME)));
            String year = (cursor.getString(cursor.getColumnIndexOrThrow(TabletopGameEntry.COLUMN_YEAR)));
            String publisher_name = (cursor.getString(cursor.getColumnIndexOrThrow(TabletopGameEntry.COLUMN_PUBLISHER)));
            String description = (cursor.getString(cursor.getColumnIndexOrThrow(TabletopGameEntry.COLUMN_DESCRIPTION)));
            String players = (cursor.getString(cursor.getColumnIndexOrThrow(TabletopGameEntry.COLUMN_PLAYERS)));
            Integer ages = (cursor.getInt(cursor.getColumnIndexOrThrow(TabletopGameEntry.COLUMN_AGES)));
            String playingTime = (cursor.getString(cursor.getColumnIndexOrThrow(TabletopGameEntry.COLUMN_PLAYINGTIME)));

            Publisher publisher = publisherDAO.get(publisher_name);

            tabletopGame = new TabletopGame(id, name, year, publisher, description, players, ages, playingTime);
            Log.i(TAG, "Tabletop game " + tabletopGame.getName() + " was retrieved from the query");
        } else {
            Log.i(TAG, "No Tabletop game was retrieved from the query");
        }
        cursor.close();
        db.close();
        return tabletopGame;
    }

    @Override
    public void save(TabletopGame tableTopGame) {
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();
        ContentValues values = tableTopGame.getValues();

        long result = db.insert(TabletopGameEntry.TABLE_NAME, null, values);
        if (result != -1)
            Log.i(TAG, "Tabletop game " + tableTopGame.getName() + " inserted into the " + TabletopGameEntry.TABLE_NAME + " table");
        else
            Log.e(TAG, "Failed to insert " + tableTopGame.getName() + " into the " + TabletopGameEntry.TABLE_NAME + " table");
        db.close();
    }

    @Override
    public void update(TabletopGame tableTopGame) {
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();
        ContentValues values = tableTopGame.getValues();
        String selection = TabletopGameEntry.COLUMN_ID + " LIKE ?";
        String[] selectionArgs = {tableTopGame.getId()};

        int result = db.update(TabletopGameEntry.TABLE_NAME, values, selection, selectionArgs);
        Log.i(TAG, result + (result == 1 ? "row" : "rows") + " updated from the " + TabletopGameEntry.TABLE_NAME + " table");
        db.close();
    }

    @Override
    public void delete(TabletopGame tableTopGame) {
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();
        String selection = TabletopGameEntry.COLUMN_ID + " LIKE ?";
        String[] selectionArgs = {tableTopGame.getId()};

        int result = db.delete(TabletopGameEntry.TABLE_NAME, selection, selectionArgs);
        Log.i(TAG, result + (result == 1 ? " row" : " rows") + " deleted from the " + TabletopGameEntry.TABLE_NAME + " table");
        db.close();
    }
}
