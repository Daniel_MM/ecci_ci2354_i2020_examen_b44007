package cr.ac.ucr.ecci.eseg.miexamen01.ui.tabletopgame;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import cr.ac.ucr.ecci.eseg.miexamen01.data.dao.TabletopGameDAO;
import cr.ac.ucr.ecci.eseg.miexamen01.data.model.TabletopGame;

public class TabletopGameViewModel extends ViewModel {

    private MutableLiveData<List<TabletopGame>> tabletopGames;

    public TabletopGameViewModel() {
        tabletopGames = new MutableLiveData<>();
    }

    void retrieveTableTopGames(TabletopGameDAO tabletopGameDAO){
        tabletopGames.setValue(tabletopGameDAO.getAll());
    }

    LiveData<List<TabletopGame>> getTableTopGames() {
        return tabletopGames;
    }
}