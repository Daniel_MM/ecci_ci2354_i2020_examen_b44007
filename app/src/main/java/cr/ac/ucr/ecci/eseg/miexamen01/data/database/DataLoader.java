package cr.ac.ucr.ecci.eseg.miexamen01.data.database;

import android.content.Context;

import cr.ac.ucr.ecci.eseg.miexamen01.data.dao.PublisherDAO;
import cr.ac.ucr.ecci.eseg.miexamen01.data.dao.TabletopGameDAO;
import cr.ac.ucr.ecci.eseg.miexamen01.data.model.Publisher;
import cr.ac.ucr.ecci.eseg.miexamen01.data.model.TabletopGame;

/**
 * Populates the data base
 *
 * @author Daniel Marín M
 */
public class DataLoader {
    public void LodaData(Context context) {
        PublisherDAO publisherDAO = new PublisherDAO(context);
        TabletopGameDAO tabletopGameDAO = new TabletopGameDAO(context);

        Publisher kosmos = new Publisher(
                "Kosmos", "Germany",
                48.774538, 9.188467
        );
        Publisher hasbro = new Publisher(
                "Hasbro", "United States",
                41.883736, -71.352259
        );
        Publisher fantasyFlightGames = new Publisher(
                "Fantasy Flight Games", "United States",
                45.015417, -93.183995
        );
        Publisher asmodee = new Publisher(
                "Asmodee", "France",
                48.761629, 2.065296
        );

        publisherDAO.save(kosmos);
        publisherDAO.save(hasbro);
        publisherDAO.save(fantasyFlightGames);
        publisherDAO.save(asmodee);

        TabletopGame catan = new TabletopGame(
                "TT001", "Catan", "1995", kosmos,
                "Picture yourself in the era of discoveries: after a long voyage of " +
                        "great deprivation, your ships have finally reached the coast of an " +
                        "uncharted island. Its name shall be Catan! But you are not the only " +
                        "discoverer. Other fearless seafarers have also landed on the shores of " +
                        "Catan: the race to settle the island has begun!",
                "3-4", 10, "1-2 hours"
        );

        TabletopGame monopoly = new TabletopGame(
                "TT002", "Monopoly", "1935", hasbro,
                "The thrill of bankrupting an opponent, but it pays to play nice, " +
                        "because fortunes could change with the roll of the dice. Experience the " +
                        "ups and downs by collecting property colors sets to build houses, and " +
                        "maybe even upgrading to a hotel! The more properties each player owns, " +
                        "the more rent can be charged. Chance cards could be worth money, or one " +
                        "might just say Go To Jail!",
                "2-8", 8, "20-180 minutes"
        );

        TabletopGame eldritchHorror = new TabletopGame(
                "TT003", "Eldritch Horror", "2013", fantasyFlightGames,
                "An ancient evil is stirring. You are part of a team of unlikely " +
                        "heroes engaged in an international struggle to stop the gathering " +
                        "darkness. To do so, you’ll have to defeat foul monsters, travel to " +
                        "Other Worlds, and solve obscure mysteries surrounding this unspeakable " +
                        "horror. The effort may drain your sanity and cripple your body, but if " +
                        "you fail, the Ancient One will awaken and rain doom upon the known world.",
                "1-8", 14, "2-4 hours"
        );

        TabletopGame magic = new TabletopGame(
                "TT004", "Magic: the Gathering", "1993", hasbro,
                "Magic: The Gathering is a collectible and digital collectible card " +
                        "game created by Richard Garfield. Each game of Magic represents a " +
                        "battle between wizards known as planeswalkers who cast spells, use " +
                        "artifacts, and summon creatures as depicted on individual cards in " +
                        "order to defeat their opponents, typically, but not always, by " +
                        "draining them of their 20 starting life points in the standard format.",
                "2+", 13, "Varies"
        );

        TabletopGame hanabi = new TabletopGame(
                "TT005", "Hanabi", "2010", asmodee,
                "Hanabi—named for the Japanese word for \"fireworks\"—is a " +
                        "cooperative game in which players try to create the perfect fireworks " +
                        "show by placing the cards on the table in the right order.",
                "2-5", 8, "25 minutes"
        );

        tabletopGameDAO.save(catan);
        tabletopGameDAO.save(monopoly);
        tabletopGameDAO.save(eldritchHorror);
        tabletopGameDAO.save(magic);
        tabletopGameDAO.save(hanabi);
    }
}
