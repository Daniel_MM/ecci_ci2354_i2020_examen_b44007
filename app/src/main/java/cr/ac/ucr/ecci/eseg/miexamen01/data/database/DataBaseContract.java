package cr.ac.ucr.ecci.eseg.miexamen01.data.database;

import android.provider.BaseColumns;

/**
 * @author Daniel Marín M
 */
public class DataBaseContract {
    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String REAL_TYPE = " REAL";
    private static final String COMMA_SEP = ",";

    private DataBaseContract() {}

    public static class PublisherEntry implements BaseColumns {
        public static final String TABLE_NAME = "Publisher";

        public static final String COLUMN_NAME = "Name";
        public static final String COLUMN_COUNTRY = "Country";
        public static final String COLUMN_LATITUDE = "Latitude";
        public static final String COLUMN_LONGITUDE = "Longitude";
    }

    public static class TabletopGameEntry implements BaseColumns {
        public static final String TABLE_NAME = "TabletopGame";

        public static final String COLUMN_ID = "Id";
        public static final String COLUMN_NAME = "Name";
        public static final String COLUMN_YEAR = "Year";
        public static final String COLUMN_PUBLISHER = "Publisher";
        public static final String COLUMN_DESCRIPTION = "Description";
        public static final String COLUMN_PLAYERS = "Players";
        public static final String COLUMN_AGES = "Ages";
        public static final String COLUMN_PLAYINGTIME = "Playingtime";
    }

    static final String SQL_CREATE_PUBLISHER =
            "CREATE TABLE " + PublisherEntry.TABLE_NAME + " (" +
                    PublisherEntry.COLUMN_NAME + TEXT_TYPE + "PRIMARY KEY" + COMMA_SEP +
                    PublisherEntry.COLUMN_COUNTRY + TEXT_TYPE + COMMA_SEP +
                    PublisherEntry.COLUMN_LATITUDE + REAL_TYPE + COMMA_SEP +
                    PublisherEntry.COLUMN_LONGITUDE + REAL_TYPE + " )";

    static final String SQL_DELETE_PUBLISHER =
            "DROP TABLE IF EXISTS " + PublisherEntry.TABLE_NAME;

    static final String SQL_CREATE_TABLE_TOP_GAME_ENTRY =
            "CREATE TABLE " + TabletopGameEntry.TABLE_NAME + " (" +
                    TabletopGameEntry.COLUMN_ID + TEXT_TYPE + "PRIMARY KEY," +
                    TabletopGameEntry.COLUMN_NAME + TEXT_TYPE + COMMA_SEP +
                    TabletopGameEntry.COLUMN_YEAR + TEXT_TYPE + COMMA_SEP +
                    TabletopGameEntry.COLUMN_PUBLISHER + TEXT_TYPE + COMMA_SEP +
                    TabletopGameEntry.COLUMN_DESCRIPTION + TEXT_TYPE + COMMA_SEP +
                    TabletopGameEntry.COLUMN_PLAYERS + TEXT_TYPE + COMMA_SEP +
                    TabletopGameEntry.COLUMN_AGES + INTEGER_TYPE + COMMA_SEP +
                    TabletopGameEntry.COLUMN_PLAYINGTIME + TEXT_TYPE + COMMA_SEP +
                    "FOREIGN KEY(" + TabletopGameEntry.COLUMN_PUBLISHER + ") " +
                    "REFERENCES " + PublisherEntry.TABLE_NAME + "(" + PublisherEntry.COLUMN_NAME + "))";

    static final String SQL_DELETE_TABLE_TOP_GAME_ENTRY =
            "DROP TABLE IF EXISTS " + TabletopGameEntry.TABLE_NAME;
}
