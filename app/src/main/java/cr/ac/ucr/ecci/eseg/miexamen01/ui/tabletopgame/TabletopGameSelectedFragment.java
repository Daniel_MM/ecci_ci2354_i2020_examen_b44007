package cr.ac.ucr.ecci.eseg.miexamen01.ui.tabletopgame;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import cr.ac.ucr.ecci.eseg.miexamen01.ui.maps.MapsFragment;
import cr.ac.ucr.ecci.eseg.miexamen01.R;
import cr.ac.ucr.ecci.eseg.miexamen01.data.model.Publisher;
import cr.ac.ucr.ecci.eseg.miexamen01.data.model.TabletopGame;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TabletopGameSelectedFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TabletopGameSelectedFragment extends Fragment implements View.OnClickListener {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String PUBLISHER = "publisher";
    private static final String TABLETOP_GAME = "TabletopGame";

    private TabletopGame tabletopGame;
    private Publisher publisher;

    public TabletopGameSelectedFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param tabletopGame the table top game to display.
     * @return A new instance of fragment TabletopGameSelectedFragment.
     */
    static TabletopGameSelectedFragment newInstance(Parcelable tabletopGame) {
        TabletopGameSelectedFragment fragment = new TabletopGameSelectedFragment();
        Bundle args = new Bundle();
        args.putParcelable(TABLETOP_GAME, tabletopGame);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            tabletopGame = getArguments().getParcelable(TABLETOP_GAME);
            if (tabletopGame != null) publisher = tabletopGame.getPublisher();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_tabletop_game_selected, container, false);
        ImageButton locationImageButton = root.findViewById(R.id.location_button);
        ImageView logoImageView = root.findViewById(R.id.tabletop_game_logo);
        TextView nameTextView = root.findViewById(R.id.name);
        TextView yearTextView = root.findViewById(R.id.year);
        TextView publisherTextView = root.findViewById(R.id.publisher);
        TextView countryTextView = root.findViewById(R.id.country);
        TextView descriptionTextView = root.findViewById(R.id.description);
        TextView playersTextView = root.findViewById(R.id.players_text);
        TextView agesTextView = root.findViewById(R.id.ages_text);
        TextView playingTimeTextView = root.findViewById(R.id.time_text);

        Context context = getActivity();
        int id = 0;
        if (context != null) {
            id = context.getResources().getIdentifier(tabletopGame.getId().toLowerCase(), "drawable", context.getPackageName());
        }
        logoImageView.setImageResource(id);
        locationImageButton.setOnClickListener(this);
        nameTextView.setText(tabletopGame.getName());
        yearTextView.setText(tabletopGame.getYear());
        publisherTextView.setText(publisher.getName());
        countryTextView.setText(publisher.getCountry());
        descriptionTextView.setText(tabletopGame.getDescription());
        playersTextView.setText(tabletopGame.getPlayers());
        String ages = tabletopGame.getAges() + "+";
        agesTextView.setText(ages);
        playingTimeTextView.setText(tabletopGame.getPlayingTime());

        return root;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.location_button) {
//            MapsFragment mapsFragment = new MapsFragment();
//            Bundle args = new Bundle();
//            args.putParcelable(PUBLISHER, publisher);
//            mapsFragment.setArguments(args);
//            getParentFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, mapsFragment).addToBackStack(null).commit();

            String geo = "geo:"+publisher.getLatitude()+","+publisher.getLongitude();
            Uri gmmIntentUri = Uri.parse(geo);
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            if (mapIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivity(mapIntent);
            }

        }
    }
}
