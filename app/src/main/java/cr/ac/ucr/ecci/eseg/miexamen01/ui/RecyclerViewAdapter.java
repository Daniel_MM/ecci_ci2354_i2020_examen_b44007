package cr.ac.ucr.ecci.eseg.miexamen01.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.AutoTransition;
import androidx.transition.TransitionManager;

import java.util.List;

import cr.ac.ucr.ecci.eseg.miexamen01.R;
import cr.ac.ucr.ecci.eseg.miexamen01.data.model.TabletopGame;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {
    private Context context;

    private List<TabletopGame> tabletopGames;

    private static OnItemClickedListener listener;

    public RecyclerViewAdapter(Context context, List<TabletopGame> tabletopGames, OnItemClickedListener listener) {
        this.context = context;
        this.tabletopGames = tabletopGames;
        RecyclerViewAdapter.listener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.cardview_item_tabletop_game, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.nameTextView.setText(tabletopGames.get(position).getName());
        holder.descriptionTextView.setText(tabletopGames.get(position).getDescription());
        holder.nPlayersTextView.setText(tabletopGames.get(position).getPlayers());
        String ages = tabletopGames.get(position).getAges()+"+";
        holder.agesTextView.setText(ages);
        holder.playingTimeTextView.setText(tabletopGames.get(position).getPlayingTime());
        int id = context.getResources().getIdentifier(tabletopGames.get(position).getId().toLowerCase(), "drawable", context.getPackageName());
        holder.logoImageView.setImageResource(id);
    }

    @Override
    public int getItemCount() {
        return tabletopGames.size();
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        CardView cardView;
        ConstraintLayout expandibleView;
        TextView nameTextView;
        TextView descriptionTextView;
        TextView nPlayersTextView;
        TextView agesTextView;
        TextView playingTimeTextView;
        ImageView logoImageView;
        ImageButton toggleButton;


        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.card_view);
            cardView.setOnClickListener(this);
            expandibleView = itemView.findViewById(R.id.expandable_layout);
            nameTextView = itemView.findViewById(R.id.tabletop_game_name);
            descriptionTextView = itemView.findViewById(R.id.tabletop_game_description);
            nPlayersTextView = itemView.findViewById(R.id.players_text);
            agesTextView = itemView.findViewById(R.id.ages_text);
            playingTimeTextView = itemView.findViewById(R.id.time_text);
            logoImageView = itemView.findViewById(R.id.tabletop_game_logo);
            toggleButton = itemView.findViewById(R.id.arrow_button);
            toggleButton.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.card_view:
                    listener.onItemClicked(getAdapterPosition());

                    break;
                case R.id.arrow_button:
                    TransitionManager.beginDelayedTransition(cardView, new AutoTransition());
                    if (expandibleView.getVisibility() == View.GONE) {
                        toggleButton.animate().setDuration(200).rotation(180);
                        expandibleView.setVisibility(View.VISIBLE);
                    } else if (expandibleView.getVisibility() == View.VISIBLE) {
                        toggleButton.animate().setDuration(200).rotation(0);
                        expandibleView.setVisibility(View.GONE);
                    }
                    break;
            }
        }
    }

    public interface OnItemClickedListener{
        void onItemClicked(int position);
    }
}
