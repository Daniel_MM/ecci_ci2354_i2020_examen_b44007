package cr.ac.ucr.ecci.eseg.miexamen01.data.model;

import android.content.ContentValues;
import android.os.Parcel;
import android.os.Parcelable;

import cr.ac.ucr.ecci.eseg.miexamen01.data.database.DataBaseContract.PublisherEntry;

/**
 * @author Daniel Marín M
 */
public class Publisher implements Parcelable {

    // Member fields

    private String name;
    private String country;
    private Double latitude;
    private Double longitude;

    // Constructors

    public Publisher() {
    }

    public Publisher(String name, String country, Double latitude, Double longitude) {
        this.name = name;
        this.country = country;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    // Parceleable implementations

    private Publisher(Parcel in) {
        name = in.readString();
        country = in.readString();
        if (in.readByte() == 0) {
            latitude = null;
        } else {
            latitude = in.readDouble();
        }
        if (in.readByte() == 0) {
            longitude = null;
        } else {
            longitude = in.readDouble();
        }
    }

    public static final Creator<Publisher> CREATOR = new Creator<Publisher>() {
        @Override
        public Publisher createFromParcel(Parcel in) {
            return new Publisher(in);
        }

        @Override
        public Publisher[] newArray(int size) {
            return new Publisher[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(country);
        if (latitude == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(latitude);
        }
        if (longitude == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(longitude);
        }
    }

    // Getters and Setters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public ContentValues getValues() {
        ContentValues values = new ContentValues();
        values.put(PublisherEntry.COLUMN_NAME, name);
        values.put(PublisherEntry.COLUMN_COUNTRY, country);
        values.put(PublisherEntry.COLUMN_LATITUDE, latitude);
        values.put(PublisherEntry.COLUMN_LONGITUDE, longitude);
        return values;
    }

    public static String[] getProjection() {
        return new String[]{
                PublisherEntry.COLUMN_NAME,
                PublisherEntry.COLUMN_COUNTRY,
                PublisherEntry.COLUMN_LATITUDE,
                PublisherEntry.COLUMN_LONGITUDE
        };
    }
}
