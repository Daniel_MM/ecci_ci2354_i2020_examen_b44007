package cr.ac.ucr.ecci.eseg.miexamen01.data.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import cr.ac.ucr.ecci.eseg.miexamen01.data.database.DataBaseContract.PublisherEntry;
import cr.ac.ucr.ecci.eseg.miexamen01.data.database.DataBaseHelper;
import cr.ac.ucr.ecci.eseg.miexamen01.data.model.Publisher;

/**
 * @author Daniel Marín M
 */
public class PublisherDAO implements DAO<Publisher> {

    private static final String TAG = "PublisherDAO";

    private DataBaseHelper dataBaseHelper;


    public PublisherDAO(Context context) {
        this.dataBaseHelper = new DataBaseHelper(context);
    }

    /**
     * Constructor usable from other DAOs
     *
     * @param dataBaseHelper initialized instance of a DataBaseHelper
     */
    PublisherDAO(DataBaseHelper dataBaseHelper) {
        this.dataBaseHelper = dataBaseHelper;
    }

    @Override
    public List<Publisher> getAll() {
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();
        List<Publisher> publishers = new ArrayList<>();
        Cursor cursor = db.query(
                PublisherEntry.TABLE_NAME,
                Publisher.getProjection(),
                null,
                null,
                null,
                null,
                null
        );

        if (cursor.moveToFirst() && cursor.getCount() > 0) {
            do {
                String name = cursor.getString(cursor.getColumnIndexOrThrow(PublisherEntry.COLUMN_NAME));
                String country = cursor.getString(cursor.getColumnIndexOrThrow(PublisherEntry.COLUMN_COUNTRY));
                Double latitude = cursor.getDouble(cursor.getColumnIndexOrThrow(PublisherEntry.COLUMN_LATITUDE));
                Double longitude = cursor.getDouble(cursor.getColumnIndexOrThrow(PublisherEntry.COLUMN_LONGITUDE));
                publishers.add(new Publisher(name, country, latitude, longitude));
            } while (cursor.moveToNext());
            Log.i(TAG, cursor.getCount() + (cursor.getCount() == 1 ? " row" : " rows") + " retrieved from query");
        } else {
            Log.i(TAG, "No publisher was retrieved from the query");
        }
        cursor.close();
        db.close();
        return publishers;
    }

    @Override
    public Publisher get(String name) {
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();
        Publisher publisher = null;
        String selection = PublisherEntry.COLUMN_NAME + " = ?";
        String[] selectionArgs = {name};
        Cursor cursor = db.query(
                PublisherEntry.TABLE_NAME,
                Publisher.getProjection(),
                selection,
                selectionArgs,
                null,
                null,
                null
        );

        if (cursor.moveToFirst() && cursor.getCount() > 0) {
            String country = cursor.getString(cursor.getColumnIndexOrThrow(PublisherEntry.COLUMN_COUNTRY));
            Double latitude = cursor.getDouble(cursor.getColumnIndexOrThrow(PublisherEntry.COLUMN_LATITUDE));
            Double longitude = cursor.getDouble(cursor.getColumnIndexOrThrow(PublisherEntry.COLUMN_LONGITUDE));
            publisher = new Publisher(name, country, latitude, longitude);
        } else {
            Log.i(TAG, "No publisher was retrieved from the query");
        }
        cursor.close();
        db.close();
        return publisher;
    }

    @Override
    public void save(Publisher publisher) {
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();
        ContentValues values = publisher.getValues();

        long result = db.insert(PublisherEntry.TABLE_NAME, null, values);
        if (result != -1)
            Log.i(TAG, "Publisher " + publisher.getName() + " inserted into the " + PublisherEntry.TABLE_NAME + " table");
        else
            Log.e(TAG, "Failed to insert " + publisher.getName() + " into the " + PublisherEntry.TABLE_NAME + " table");
        db.close();
    }

    @Override
    public void update(Publisher publisher) {
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();
        ContentValues values = publisher.getValues();
        String selection = PublisherEntry.COLUMN_NAME + " LIKE ?";
        String[] selectionArgs = {publisher.getName()};

        int result = db.update(PublisherEntry.TABLE_NAME, values, selection, selectionArgs);
        Log.i(TAG, result + (result == 1 ? "row" : "rows") + " updated from the " + PublisherEntry.TABLE_NAME + " table");
        db.close();
    }

    @Override
    public void delete(Publisher publisher) {
        SQLiteDatabase db = dataBaseHelper.getWritableDatabase();
        String selection = PublisherEntry.COLUMN_NAME + " LIKE ?";
        String[] selectionArgs = {publisher.getName()};

        int result = db.delete(PublisherEntry.TABLE_NAME, selection, selectionArgs);
        Log.i(TAG, result + (result == 1 ? " row" : " rows") + " deleted from the " + PublisherEntry.TABLE_NAME + " table");
        db.close();
    }
}
