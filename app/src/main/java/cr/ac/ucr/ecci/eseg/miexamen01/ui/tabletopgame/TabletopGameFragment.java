package cr.ac.ucr.ecci.eseg.miexamen01.ui.tabletopgame;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import cr.ac.ucr.ecci.eseg.miexamen01.R;
import cr.ac.ucr.ecci.eseg.miexamen01.data.dao.TabletopGameDAO;
import cr.ac.ucr.ecci.eseg.miexamen01.data.model.TabletopGame;
import cr.ac.ucr.ecci.eseg.miexamen01.ui.RecyclerViewAdapter;

public class TabletopGameFragment extends Fragment implements Observer<List<TabletopGame>>, View.OnClickListener, RecyclerViewAdapter.OnItemClickedListener {

    private TabletopGameViewModel tabletopGameViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        tabletopGameViewModel = new ViewModelProvider(this).get(TabletopGameViewModel.class);
        tabletopGameViewModel.retrieveTableTopGames(new TabletopGameDAO(getActivity()));
        View root = inflater.inflate(R.layout.fragment_tabletop_game, container, false);

        RecyclerView recyclerView = root.findViewById(R.id.recyclerview_id);
        RecyclerViewAdapter recyclerViewAdapter = new RecyclerViewAdapter(getActivity(), tabletopGameViewModel.getTableTopGames().getValue(),this);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(recyclerViewAdapter);

        tabletopGameViewModel.getTableTopGames().observe(getViewLifecycleOwner(), this);

        return root;
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onChanged(List<TabletopGame> tabletopGames) {

    }

    @Override
    public void onItemClicked(int position) {
        List<TabletopGame> tabletopGames = tabletopGameViewModel.getTableTopGames().getValue();
        TabletopGame tabletopGame = tabletopGames != null? tabletopGames.get(position) : null;
        TabletopGameSelectedFragment tabletopGameSelectedFragment = TabletopGameSelectedFragment.newInstance(tabletopGame);
        getParentFragmentManager().beginTransaction().replace(R.id.nav_host_fragment, tabletopGameSelectedFragment).addToBackStack(null).commit();
    }


}
