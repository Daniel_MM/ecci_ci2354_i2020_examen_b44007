package cr.ac.ucr.ecci.eseg.miexamen01.data.dao;

import java.util.List;

/**
 * @param <T> the model class to work with
 * @author Daniel Marín M
 */
public interface DAO<T> {

    /**
     * Gets all the T objects from the corresponding table.
     *
     * @return a list with all the T objects on the table.
     */
    List<T> getAll();

    /**
     * Gets a T object by its id from the corresponding table.
     *
     * @param id the primary key of the object to retrieve.
     * @return the object that matches the given id.
     */
    T get(String id);

    /**
     * Saves a T object into the corresponding table.
     *
     * @param t the complete T object to save.
     */
    void save(T t);

    /**
     * Updates a T object from the corresponding table.
     *
     * @param t the altered T object with an existing id to be updated.
     */
    void update(T t);

    /**
     * deletes a T object from the corresponding table.
     *
     * @param t the T object with an existing id to be deleted.
     */
    void delete(T t);
}
