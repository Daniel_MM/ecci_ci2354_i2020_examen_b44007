package cr.ac.ucr.ecci.eseg.miexamen01.data.model;

import android.content.ContentValues;
import android.os.Parcel;
import android.os.Parcelable;

import cr.ac.ucr.ecci.eseg.miexamen01.data.database.DataBaseContract.TabletopGameEntry;

/**
 * @author Daniel Marín M
 */
public class TabletopGame implements Parcelable {

    // Member fields

    private String id;
    private String name;
    private String year;
    private Publisher publisher;
    private String description;
    private String players;
    private Integer ages;
    private String playingTime;

    // Constructors

    public TabletopGame() {
    }

    public TabletopGame(String id, String name, String year, Publisher publisher, String description, String players, Integer ages, String playingTime) {
        this.id = id;
        this.name = name;
        this.year = year;
        this.publisher = publisher;
        this.description = description;
        this.players = players;
        this.ages = ages;
        this.playingTime = playingTime;
    }

    // Parceleable implementations

    private TabletopGame(Parcel in) {
        id = in.readString();
        name = in.readString();
        year = in.readString();
        publisher = in.readParcelable(Publisher.class.getClassLoader());
        description = in.readString();
        players = in.readString();
        ages = in.readByte() == 0 ? null : in.readInt();
        playingTime = in.readString();
    }

    public static final Creator<TabletopGame> CREATOR = new Creator<TabletopGame>() {
        @Override
        public TabletopGame createFromParcel(Parcel in) {
            return new TabletopGame(in);
        }

        @Override
        public TabletopGame[] newArray(int size) {
            return new TabletopGame[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(year);
        dest.writeParcelable(publisher, flags);
        dest.writeString(description);
        dest.writeString(players);
        if (ages == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(ages);
        }
        dest.writeString(playingTime);
    }

    // Getters and Setters

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPlayers() {
        return players;
    }

    public void setPlayers(String players) {
        this.players = players;
    }

    public Integer getAges() {
        return ages;
    }

    public void setAges(Integer ages) {
        this.ages = ages;
    }

    public String getPlayingTime() {
        return playingTime;
    }

    public void setPlayingTime(String playingTime) {
        this.playingTime = playingTime;
    }

    public ContentValues getValues() {
        ContentValues values = new ContentValues();
        values.put(TabletopGameEntry.COLUMN_ID, id);
        values.put(TabletopGameEntry.COLUMN_NAME, name);
        values.put(TabletopGameEntry.COLUMN_YEAR, year);
        values.put(TabletopGameEntry.COLUMN_PUBLISHER, publisher.getName());
        values.put(TabletopGameEntry.COLUMN_DESCRIPTION, description);
        values.put(TabletopGameEntry.COLUMN_PLAYERS, players);
        values.put(TabletopGameEntry.COLUMN_AGES, ages);
        values.put(TabletopGameEntry.COLUMN_PLAYINGTIME, playingTime);
        return values;
    }

    public static String[] getProjection() {
        return new String[]{
                TabletopGameEntry.COLUMN_ID,
                TabletopGameEntry.COLUMN_NAME,
                TabletopGameEntry.COLUMN_YEAR,
                TabletopGameEntry.COLUMN_PUBLISHER,
                TabletopGameEntry.COLUMN_DESCRIPTION,
                TabletopGameEntry.COLUMN_PLAYERS,
                TabletopGameEntry.COLUMN_AGES,
                TabletopGameEntry.COLUMN_PLAYINGTIME
        };
    }

}
